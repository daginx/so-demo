const DemoCoin = artifacts.require("DemoCoin");

contract("DemoCoin", (accounts) => {
  const decimals = 18;
  const initAccount = "0xafB1ef5DF791d4FF2ac5E80f71Ed630F7a8EC1a4";

  beforeEach("setup contract", async function () {
    const initialBalance = "1000000000" + "0".repeat(decimals);
    const demoParams = [
      "DemoCoin",
      "DEMO",
      decimals,
      initAccount,
      initialBalance,
    ];
    this.token = await DemoCoin.deployed(demoParams);
  });

  describe("Test something", async function () {
    it("init", async function () {
      const getDecimals = await this.token.decimals.call();
      assert.equal(getDecimals.valueOf(), decimals, "decimals wrong");
    });

    it("ownable", async function () {
      const getOwner = await this.token.owner.call();
      assert.equal(getOwner.valueOf(), initAccount, "owner wrong");
    });
  });
});
