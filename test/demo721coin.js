const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const Demo721Coin = artifacts.require("Demo721Coin");

contract("Demo721Coin", (accounts) => {
  const [
    owner,
    newOwner,
    approved,
    anotherApproved,
    operator,
    other,
  ] = accounts;
  const name = "Non Fungible Token";
  const symbol = "NFT";
  const firstTokenId = new BN("5042");
  const secondTokenId = new BN("79217");
  const nonExistentTokenId = new BN("13");

  beforeEach(async function () {
    this.token = await Demo721Coin.new(name, symbol);
  });

  describe("token URI", function () {
    it("mint", async function () {
      await this.token.mint(owner, firstTokenId);
    });
  });

  describe("IERC721Enumerable", function () {
    it("try", async function () {
      await this.token.mint(owner, firstTokenId);
      const totalSupply = await this.token.totalSupply.call();
      assert.equal(totalSupply.valueOf(), 1, "totalSupply wrong");
    });
  });
});
