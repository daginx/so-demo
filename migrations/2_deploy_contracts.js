const DemoCoin = artifacts.require("DemoCoin");
const Demo721Coin = artifacts.require("Demo721Coin");

module.exports = function (deployer) {
  const decimals = 18;
  const initialBalance = "1000000000" + "0".repeat(decimals);
  const demoParams = [
    "DemoCoin",
    "DEMO",
    decimals,
    "0xafB1ef5DF791d4FF2ac5E80f71Ed630F7a8EC1a4",
    initialBalance,
  ];

  deployer.deploy(DemoCoin, ...demoParams);
  deployer.deploy(Demo721Coin, "Demo721", "D721");
};
